call plug#begin('~/.config/nvim/autoload/plug')
	Plug 'jiangmiao/auto-pairs'                                                             "Plugins pour autofermé les cotes, parenthése etc"
  " Plug 'joshdick/onedark.vim'                                                           "Plugins theme couleur"
  " Plug 'navarasu/onedark.nvim'
  " Plug 'tomasiser/vim-code-dark'                                                        "Plugins theme couleur"
  " Plug 'doums/darcula'                                                                  "Plugins theme couleur"
  Plug 'lifepillar/vim-solarized8'
  Plug 'vim-airline/vim-airline'                                                          "Plugins pour modifier le style des mode et des buffers"
  Plug 'vim-airline/vim-airline-themes'
  Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}  "PLugins pour l'autocompletion intelissense" 
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }                                     "Plugins pour naviguer dans les fichiers"
  Plug 'junegunn/fzf.vim'                                                                 "Plugin pour utiliser fzf dans vim"
  Plug 'airblade/vim-rooter'                                                              "Plugin pour changer automatiquement de directory quand on ouvre un fichier"
  Plug 'mhinz/vim-startify'                                                               "Menu d'accueil de nvim"
  Plug 'mhinz/vim-signify'                                                                "Plugin pour git dans nvim permet de voir les fichiers et lignes ajouter, supprimer, modifier par rapport au dépot distant"
  Plug 'tpope/vim-commentary'                                                             "Plugin pour commenter un/des ligne"
  Plug 'yuezk/vim-js'                                                                     "Plugin pour javascipt completion"
  Plug 'maxmellon/vim-jsx-pretty'                                                         "Plugin for jsx"
  Plug 'OmniSharp/omnisharp-vim'                                                          "Plugin for dotnet c#"
call plug#end()
