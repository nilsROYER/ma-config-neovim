source $HOME/.config/nvim/vim-plug/plugins.vim "Gestionnaire de pluggins vim"
source $HOME/.config/nvim/general/settings.vim "Seeting de neovim"
source $HOME/.config/nvim/keys/mappings.vim "Raccourcie clavier pour neovim"
" source $HOME/.config/nvim/themes/onedark.vim "Theme couleur pour neovim"
" source $HOME/.config/nvim/themes/darcula.vim "Theme couleur pour neovim"
" source $HOME/.config/nvim/themes/vscode.vim "Theme couleur pour neovim"
source $HOME/.config/nvim/themes/solarized8.vim "Theme couleur pour neovim"
source $HOME/.config/nvim/themes/airline.vim "Theme couleur pour les mode dans vim (insert, normal, visuel, terminal)"
source $HOME/.config/nvim/plug-config/coc.vim "Seeting de Coc pluggins d'autocompletion et d'intelissense"
source $HOME/.config/nvim/plug-config/fzf.vim "Seeting de fzf pluggins pour naviger dans les fichier"
source $HOME/.config/nvim/plug-config/project-start.vim "Fichier de configuration pour le pluggins startify"


