Pour installer ma config tout d'abord installer neovim ==>  [ici](https://github.com/neovim/neovim/wiki/Installing-Neovim)

Ensuite pour Linux:

``` bash
mkdir ~/.config/nvim
cd ~/.config/nvim
git clone https://gitlab.com/nilsROYER/ma-config-neovim.git ./
```
Windows: 

``` bash
mkdir ~/AppData/Local/nvim
cd ~/AppData/Local/nvim
git clone https://gitlab.com/nilsROYER/ma-config-neovim.git ./
```

